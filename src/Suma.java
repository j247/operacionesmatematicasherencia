package src;

public class Suma extends OperacionesMat {

	public Suma(double numero1, double numero2) {
		super(numero1, numero2);		
	}
	
	public double getResultado() {
		return super.getNumero1() + super.getNumero2();
	}

	@Override
	public String toString() {
		return "" + this.getResultado();				
	}
}

package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import src.*;

public class OperacionesMatematicas extends JFrame {

	private JPanel contentPane;
	private JTextField tf_num1;
	private JTextField tf_num2;
	private JTextField tf_resultado;
	String resul;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OperacionesMatematicas frame = new OperacionesMatematicas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OperacionesMatematicas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Operaci\u00F3n Matem\u00E1tica");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setBounds(92, 11, 249, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Primer d\u00EDgito");
		lblNewLabel_1.setBounds(36, 53, 114, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Segundo d\u00EDgito");
		lblNewLabel_2.setBounds(32, 93, 118, 14);
		contentPane.add(lblNewLabel_2);
		
		tf_num1 = new JTextField();
		tf_num1.setBounds(176, 50, 86, 20);
		contentPane.add(tf_num1);
		tf_num1.setColumns(10);
		
		tf_num2 = new JTextField();
		tf_num2.setBounds(176, 90, 86, 20);
		contentPane.add(tf_num2);
		tf_num2.setColumns(10);
		
		tf_resultado = new JTextField();
		tf_resultado.setEditable(false);
		tf_resultado.setBounds(220, 230, 86, 20);
		contentPane.add(tf_resultado);
		tf_resultado.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Resultado");
		lblNewLabel_3.setBounds(92, 233, 90, 14);
		contentPane.add(lblNewLabel_3);
		//radioB
		JRadioButton rd_sumar = new JRadioButton("Sumar");
		rd_sumar.setSelected(true);
		rd_sumar.setBounds(32, 147, 86, 23);
		contentPane.add(rd_sumar);
		
		JRadioButton rd_restar = new JRadioButton("Restar");
		rd_restar.setBounds(120, 147, 86, 23);
		contentPane.add(rd_restar);
		
		JRadioButton rd_multi = new JRadioButton("Multiplicar");
		rd_multi.setBounds(208, 147, 109, 23);
		contentPane.add(rd_multi);
		
		JRadioButton rd_division = new JRadioButton("Divisi\u00F3n");
		rd_division.setBounds(316, 147, 109, 23);
		contentPane.add(rd_division);
		
		ButtonGroup g1 = new ButtonGroup();
		g1.add(rd_sumar);
		g1.add(rd_restar);
		g1.add(rd_multi);
		g1.add(rd_division);
		
		JButton btn_salir = new JButton("Salir");
		btn_salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btn_salir.setBackground(Color.RED);
		btn_salir.setBounds(43, 188, 89, 23);
		contentPane.add(btn_salir);
		
		JButton btn_limpiar = new JButton("Limpiar");
		btn_limpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tf_num1.setText("");
				tf_num2.setText("");
				tf_resultado.setText("");				
			}
		});
		btn_limpiar.setBounds(183, 188, 89, 23);
		contentPane.add(btn_limpiar);
		
		JButton btn_calcular = new JButton("Calcular");
		btn_calcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
								
					double num1= Double.parseDouble(tf_num1.getText());
					double num2= Double.parseDouble(tf_num2.getText());
					if (rd_sumar.isSelected() == true) {
						Suma s = new Suma(num1,num2);
						resul = s.toString();
					}
					if (rd_restar.isSelected() == true) {
						Resta r = new Resta(num1,num2);
						resul = r.toString();
					}
					if (rd_multi.isSelected() == true) {
						Producto p = new Producto(num1,num2);
						resul = p.toString();
					}
					if (rd_division.isSelected() == true) {
						Division d = new Division(num1,num2);
						resul = d.toString();
					}			
					tf_resultado.setText("" + resul);									
			}			
			
		});
		btn_calcular.setBackground(new Color(0, 128, 0));
		btn_calcular.setBounds(312, 188, 89, 23);
		contentPane.add(btn_calcular);
		
		
	}

	

}
